import axios from "axios";

const xhrBase = () => {
  const xhr = axios.create();
  return xhr;
};

export const xhr = xhrBase();
