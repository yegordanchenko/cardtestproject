import { xhr } from "../baseConfig";

const url = "http://localhost:3333/v1/cards";

export const getCardsList = () => xhr.get(url);

export const createCard = (body: { [key: string]: any }) => {
  return xhr.post(url, body);
};

export const getCard = (id: string) => xhr.get(`${url}/${id}`);
