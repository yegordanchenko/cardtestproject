import { useEffect, useCallback, useState } from "react";
import { AxiosError, AxiosResponse, AxiosPromise } from "axios";

interface UseFetchOptions {
  fetchOnMount?: boolean;
  mountParams?: any[];
}

type TupelizeResponseType<T> = Promise<
  [AxiosError<T> | null, AxiosResponse<T> | null]
>;

type FetchDataType<RequestDataType> =
  | ((...args: any[]) => AxiosPromise<RequestDataType> | Promise<void | any>)
  | (() => AxiosPromise<RequestDataType> | Promise<void | any>);

type TupelizePromiseType = (
  promise: AxiosPromise<any>
) => TupelizeResponseType<any>;

export const tupelizePromise: TupelizePromiseType = (promise) => {
  return new Promise<any>((resolve) =>
    promise.then(
      (data) => {
        return resolve([null, data]);
      },
      (err) => {
        resolve([err, null]);
      }
    )
  );
};

interface UseFetchType {
  <RequestDataType = any>(
    promiseFunction: FetchDataType<RequestDataType>,
    mountOptionsProps?: UseFetchOptions
  ): [
    {
      data: any;
      loading: boolean;
      error: any;
      response: AxiosResponse<RequestDataType>;
    },
    FetchDataType<RequestDataType>,
    React.Dispatch<any>
  ];
}

export const useFetch: UseFetchType = (promiseFunction, fetchParams) => {
  const { fetchOnMount, mountParams = [] } = fetchParams || {};

  const [data, setData] = useState<null | AxiosResponse>(null);
  const [res, setRes] = useState<any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const [errorState, setErrorState] = useState<null | AxiosError>(null);

  const fetchCallback = useCallback(
    async (...params) => {
      setLoading(true);

      const [error, response] = await tupelizePromise(
        promiseFunction(...params)
      );

      if (error && !response) {
        setErrorState(error);
        setLoading(false);
        return [error, null];
      }

      let respData = null;
      respData = response?.data;

      setErrorState(null);
      setRes(response);
      setData(respData);
      setLoading(false);
      return [null, respData];
    },
    [setLoading, promiseFunction]
  );

  useEffect(() => {
    if (fetchOnMount) {
      fetchCallback(...mountParams);
    }
  }, [fetchCallback]);

  return [
    {
      data,
      loading,
      error: errorState,
      response: res,
    },
    fetchCallback,
    setData,
  ];
};
