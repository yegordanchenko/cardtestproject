import Typography from "@mui/material/Typography";
import { Layout } from "../../layout/Layout";

export const Home = () => {
  return (
    <Layout title="Home">
      <Typography mb="2">Welcome!</Typography>
      <Typography>
        To create a new card info open menu and click "Create Card"
      </Typography>
    </Layout>
  );
};
