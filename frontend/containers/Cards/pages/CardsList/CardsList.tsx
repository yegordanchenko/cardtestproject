import { FC, useMemo } from "react";
import { useRouter } from "next/router";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { DataGrid, GridColDef, GridRowParams } from "@mui/x-data-grid";

import { Layout } from "../../../../layout";

type Card = {
  id: string;
  name: string;
  cardNumber: string;
  nickname: string;
  cvv: number;
  expiredMonth: number;
  expiredYear: number;
};

type CardsListType = {
  data: Card[];
};

export const CardsList: FC<CardsListType> = ({ data = [] }) => {
  const { push } = useRouter();

  const renderDatas = useMemo(() => {
    if (!data.length) {
      return (
        <>
          <Typography>There are no created cards</Typography>
          <br />
          <Button onClick={() => push("/cards/create")}>Create new card</Button>
        </>
      );
    }

    const columns: GridColDef[] = [
      {
        field: "cardNumber",
        headerName: "Card number",
        type: "number",
        width: 150,
      },
      {
        field: "cardType",
        headerName: "Card type",
        width: 150,
      },
      {
        field: "name",
        headerName: "Name",
        width: 150,
      },
      {
        field: "nickname",
        headerName: "Nickname",
        width: 150,
      },
      {
        field: "cvv",
        headerName: "Cvv",
        type: "number",
        width: 100,
      },
      {
        field: "expiredMonth",
        headerName: "Expired month",
        type: "number",
        width: 150,
      },
      {
        field: "expiredYear",
        headerName: "Expired year",
        type: "number",
        width: 150,
      },
    ];

    return (
      <div>
        <DataGrid
          rows={data}
          columns={columns}
          hideFooterPagination
          onRowClick={({ id }: GridRowParams) => push(`/cards/${id}`)}
        />
      </div>
    );
  }, [data]);

  return <Layout title="Cards list">{renderDatas}</Layout>;
};
