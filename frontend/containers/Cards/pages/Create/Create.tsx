import { GeneralInfo } from "../GeneralInfo";
import { Layout } from "../../../../layout";

export const Create = () => {
  return (
    <Layout title="Create Card Form">
      <GeneralInfo />
    </Layout>
  );
};
