export const monthsOptions = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
].map((name: string, index: number) => ({ id: index + 1, name }));

export const yearsOptions = [2019, 2020, 2021, 2022, 2023].map(
  (val: number) => ({
    id: val,
    name: val,
  })
);
