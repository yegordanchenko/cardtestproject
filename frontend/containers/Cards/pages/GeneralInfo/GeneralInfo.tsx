import { useEffect, useState, useCallback } from "react";
import { Form, Field } from "react-final-form";
import { useRouter } from "next/router";
import creditCardType from "credit-card-type";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import FormHelperText from "@mui/material/FormHelperText";
import { yearsOptions, monthsOptions } from "./utils";
import { useFetch } from "../../../../hooks/useFetch";
import { createCard, getCard } from "../../../../api/cards";

const DEFAULT_REQUIRED_MESSAGE = "Required field";
const fullWidth = { width: "100%" };

export const GeneralInfo = () => {
  const {
    query: { id },
    push,
  } = useRouter();

  const isCreate = !id;

  const [initialValues, setInitialValues] = useState<{ [key: string]: any }>(
    {}
  );

  const [, createCallback] = useFetch(createCard, {
    fetchOnMount: false,
  });

  const [{ data }, getCardCallback] = useFetch(getCard, {
    fetchOnMount: false,
  });

  useEffect(() => {
    if (id) {
      getCardCallback(id);
    }
  }, [id]);

  useEffect(() => {
    if (data) {
      setInitialValues(data);
    }
  }, [data]);

  const submitForm = useCallback(async (values: { [key: string]: any }) => {
    const [error, data] = await createCallback(values);

    if (!error && data) {
      push("/cards");
    }
  }, []);

  const validateCardNumber = useCallback((value: string) => {
    if (!value) {
      return DEFAULT_REQUIRED_MESSAGE;
    }

    const regex = /^\d+$/;

    const onlyDigits = regex.test(value);

    if (!onlyDigits) {
      return "There should be only digits";
    }

    if (value.length < 12 || value.length > 19) {
      return "Incorrect card length";
    }
  }, []);

  const validateCvv = useCallback((value: string) => {
    if (!value) {
      return DEFAULT_REQUIRED_MESSAGE;
    }

    const regex = /^\d+$/;

    const onlyDigits = regex.test(value);

    if (!onlyDigits) {
      return "There should be only digits";
    }

    if (!value.length || value.length > 4) {
      return "Incorrect card length";
    }
  }, []);

  const validateValue = useCallback(
    (value: string) => (value ? undefined : DEFAULT_REQUIRED_MESSAGE),
    []
  );

  return (
    <Container>
      <Form
        onSubmit={submitForm}
        initialValues={initialValues}
        render={({ handleSubmit, values: { cardNumber } }) => {
          const cardTypeValue = creditCardType(cardNumber)[0];

          return (
            <form onSubmit={handleSubmit}>
              <Box mb={2}>
                <Field name="cardNumber" validate={validateCardNumber}>
                  {({
                    input: { value, onChange },
                    meta: { error, touched },
                  }) => {
                    return (
                      <TextField
                        error={touched && Boolean(error)}
                        label="Card Number"
                        inputProps={{
                          inputMode: "numeric",
                          pattern: "[0-9]*",
                        }}
                        helperText={
                          touched && Boolean(error) ? error : undefined
                        }
                        value={value}
                        onChange={onChange}
                        sx={fullWidth}
                        disabled={!isCreate}
                      />
                    );
                  }}
                </Field>
              </Box>
              <Box mb={2}>
                <Field name="name" validate={validateValue}>
                  {({
                    input: { value, onChange },
                    meta: { error, touched },
                  }) => {
                    return (
                      <TextField
                        error={touched && Boolean(error)}
                        label="Name"
                        helperText={
                          touched && Boolean(error) ? error : undefined
                        }
                        value={value}
                        onChange={onChange}
                        sx={fullWidth}
                        disabled={!isCreate}
                      />
                    );
                  }}
                </Field>
              </Box>
              <Box
                mt={2}
                sx={{ display: "flex", justifyContent: "space-between" }}
              >
                <Box mr={2} sx={{ display: "flex" }}>
                  <Box mr={1} sx={{ minWidth: 160 }}>
                    <Field name="expiredMonth" validate={validateCardNumber}>
                      {({
                        input: { value, onChange },
                        meta: { error, touched },
                      }) => {
                        return (
                          <FormControl fullWidth>
                            <InputLabel
                              id="expiredMonth"
                              error={touched && Boolean(error)}
                            >
                              Expired month
                            </InputLabel>
                            <Select
                              value={value}
                              label="Expired month"
                              onChange={onChange}
                              labelId="expiredMonth"
                              error={touched && Boolean(error)}
                              disabled={!isCreate}
                            >
                              {monthsOptions.map(({ id, name }) => (
                                <MenuItem value={id} key={id}>
                                  {name}
                                </MenuItem>
                              ))}
                            </Select>
                            {touched && Boolean(error) && (
                              <FormHelperText error={touched && Boolean(error)}>
                                {error}
                              </FormHelperText>
                            )}
                          </FormControl>
                        );
                      }}
                    </Field>
                  </Box>
                  <Box mr={1} sx={{ minWidth: 160 }}>
                    <Field name="expiredYear" validate={validateCardNumber}>
                      {({
                        input: { value, onChange },
                        meta: { error, touched },
                      }) => {
                        return (
                          <FormControl fullWidth>
                            <InputLabel
                              id="expiredYear"
                              error={touched && Boolean(error)}
                            >
                              Expired year
                            </InputLabel>
                            <Select
                              value={value}
                              label="Expired year"
                              onChange={onChange}
                              labelId="expiredYear"
                              error={touched && Boolean(error)}
                              disabled={!isCreate}
                            >
                              {yearsOptions.map(({ id, name }) => (
                                <MenuItem value={id} key={id}>
                                  {name}
                                </MenuItem>
                              ))}
                            </Select>
                            {touched && Boolean(error) && (
                              <FormHelperText error={touched && Boolean(error)}>
                                {error}
                              </FormHelperText>
                            )}
                          </FormControl>
                        );
                      }}
                    </Field>
                  </Box>
                </Box>

                <Field name="cvv" validate={validateCvv}>
                  {({
                    input: { value, onChange },
                    meta: { error, touched },
                  }) => {
                    return (
                      <TextField
                        error={touched && Boolean(error)}
                        label="CVV"
                        helperText={
                          touched && Boolean(error) ? error : undefined
                        }
                        value={value}
                        onChange={onChange}
                        disabled={!isCreate}
                      />
                    );
                  }}
                </Field>
              </Box>
              <Box mt={2}>
                <TextField
                  label="Card Type"
                  value={cardTypeValue ? cardTypeValue.niceType : ""}
                  disabled
                />
              </Box>
              <Box mt={2}>
                <Field name="nickname">
                  {({ input: { value, onChange } }) => {
                    return (
                      <TextField
                        label="Nickname"
                        value={value}
                        onChange={onChange}
                        sx={fullWidth}
                        disabled={!isCreate}
                      />
                    );
                  }}
                </Field>
              </Box>

              <Box mt={2}>
                <Button type="submit" disabled={!isCreate}>
                  Create
                </Button>
              </Box>
            </form>
          );
        }}
      />
    </Container>
  );
};
