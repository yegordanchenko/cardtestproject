import type { NextPage } from "next";
import Head from "next/head";
import { Home } from "../containers/Home";

const HomePage: NextPage = () => {
  return (
    <div>
      <Head>
        <title>CardApp</title>
        <meta name="description" content="Test CardApp" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>
        <Home />
      </div>
    </div>
  );
};

export default HomePage;
