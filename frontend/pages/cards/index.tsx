import type { NextPage } from "next";
import Head from "next/head";
import { CardsList } from "../../containers/Cards/pages";
import { getCardsList } from "../../api/cards";

type CardType = {};

const CardsIndex: NextPage<{ data: CardType[] }> = ({ data = [] }) => {
  return (
    <div>
      <Head>
        <title>Cards list | CardApp</title>
        <meta name="description" content="Cards list | CardApp" />
      </Head>
      <div>
        <CardsList data={data} />
      </div>
    </div>
  );
};

export default CardsIndex;

export const getStaticProps = async () => {
  let data = [];

  try {
    const { data: responseData } = await getCardsList();
    data = responseData;
  } catch (error) {
    console.error(error);
  }

  return {
    props: { data },
  };
};
