import Head from "next/head";
import { Create } from "../../containers/Cards/pages";

const CardsIndex = () => {
  return (
    <div>
      <Head>
        <title>Create card form | CardApp</title>
        <meta name="description" content="Cards list | CardApp" />
      </Head>
      <div>
        <Create />
      </div>
    </div>
  );
};

export default CardsIndex;
