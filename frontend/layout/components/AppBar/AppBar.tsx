import { FC, useCallback, useState } from "react";
import MuiAppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import { AppDrawer } from "../AppDrawer";

type AppBarProps = {
  title: string;
};

export const AppBar: FC<AppBarProps> = ({ title }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const openDrawer = useCallback(() => setIsOpen(true), []);

  return (
    <Box sx={{ flexGrow: 1 }}>
      <MuiAppBar position="static">
        <AppDrawer isOpen={isOpen} setIsOpen={setIsOpen} />
        <Toolbar variant="dense">
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={openDrawer}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" color="inherit" component="div">
            {title}
          </Typography>
        </Toolbar>
      </MuiAppBar>
    </Box>
  );
};
