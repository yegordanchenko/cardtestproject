import { FC, useCallback } from "react";
import { useRouter } from "next/router";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";

type AppDrawerProps = {
  isOpen: boolean;
  setIsOpen: (state: boolean) => void;
};

export const AppDrawer: FC<AppDrawerProps> = ({ isOpen, setIsOpen }) => {
  const closeDrawer = useCallback(() => setIsOpen(false), []);
  const { push } = useRouter();

  return (
    <Drawer anchor="left" open={isOpen} onClose={closeDrawer}>
      <List>
        <ListItem button onClick={() => push("/")}>
          <ListItemText primary="Home" />
        </ListItem>
        <ListItem button onClick={() => push("/cards")}>
          <ListItemText primary="Cards List" />
        </ListItem>
        <ListItem button onClick={() => push("/cards/create")}>
          <ListItemText primary="Create Card" />
        </ListItem>
      </List>
    </Drawer>
  );
};
