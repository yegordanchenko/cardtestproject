import { FC } from "react";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import { AppBar } from "./components/AppBar";

type LayoutProps = {
  title: string;
};

export const Layout: FC<LayoutProps> = ({ children, title }) => {
  return (
    <>
      <AppBar title={title} />
      <Container>
        <Box mt={2}>{children}</Box>
      </Container>
    </>
  );
};
