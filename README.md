Description: Small test project with cradit card app

Under the hood:
React.js
Next.js
Node.js
Express.js
MongoDB

Start of project:

0. Open docker folder.
1. Run command `docker-compose up -d`.
2. Open server directory.
3. Run command `npm run start`.
4. Open Frontend directory.
5. Run command `npm run dev`.
6. Open in Browser address http://localhost:3000

Enjoy.
