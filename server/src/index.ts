import db from "./app/db";
db();

import express from "express";
import bodyParser from "body-parser";
import routesV1 from "./app/routes/v1";

const app = express();

const PORT = process.env.PORT || 3333;
const NODE_ENV = process.env.NODE_ENV;

app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));

// FOR DEV PURPOSES ONLY
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,Access-Control-Allow-Origin"
  );
  res.setHeader("Access-Control-Allow-Credentials", "true");

  next();
});
// FOR DEV PURPOSES ONLY

app.use("/v1", routesV1);

app.use(function (req, res, next) {
  res.status(404);
  if (req.accepts("json")) {
    return res.send({ message: "Not found" });
  }
  res.type("txt").send("Not found");
});

app.listen(PORT, () => {
  console.log("Server started on port " + PORT + " !");
});
