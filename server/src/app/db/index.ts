import mongoose from "mongoose";

const {
  MONGO_LOGIN = "testApp",
  MONGO_PASSWORD = "testAppPassword",
  MONGO_ADDRESS = "localhost",
  MONGO_PORT = 27019,
  MONGO_DB_NAME = "cardTestApp",
} = process.env;

export default function () {
  mongoose.connection.on("connected", function () {
    console.log("Connected to DB at - ", new Date());
  });

  mongoose.connection.on("error", function (err) {
    console.log("There are some error while connecting to DB: " + err);
  });

  mongoose.connection.on("disconnected", function () {
    console.log("Disconnected from DB - ", new Date());
  });

  process.on("SIGINT", function () {
    mongoose.connection.close(function () {
      console.log("SIGINT received -", new Date());
      process.exit(0);
    });
  });

  mongoose.connect(
    `mongodb://${MONGO_LOGIN}:${MONGO_PASSWORD}@${MONGO_ADDRESS}:${MONGO_PORT}/${MONGO_DB_NAME}`
  );

  return mongoose.connection;
}
