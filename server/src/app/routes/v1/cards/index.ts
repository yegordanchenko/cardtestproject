import express from "express";
import cardController from "../../../controllers/v1/card";

const router = express.Router();

router.get("/", cardController.getCardList);
router.post("/", cardController.createCard);
router.get("/:_id", cardController.getCard);

export default router;
