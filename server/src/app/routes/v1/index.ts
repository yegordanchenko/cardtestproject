import express from "express";
import cards from "./cards";

const router = express.Router();

router.use("/cards", cards);

export default router;
