import creditCardType from "credit-card-type";
import { Card } from "../../../models";

const getCardList = async (req, res) => {
  Card.find()
    .then((data) => {
      res.status(200).json(hideCardInfo(data));
    })
    .catch((error) => {
      res.status(400).json({ error });
    });
};

const createCard = async (req, res) => {
  const { body } = req;

  Card.create(body)
    .then((response) => {
      res.status(200).json(response);
    })
    .catch((error) => {
      res.status(400).json({ error });
    });
};

const getCard = async (req, res) => {
  const {
    params: { _id },
  } = req;

  Card.findOne({ _id })
    .then((response) => {
      res.status(200).json(hideCardInfo(response));
    })
    .catch((error) => {
      res.status(400).json({ error });
    });
};

const hideCardInfo = (
  info: {
    cardNumber: string;
    createdAt: Date;
    updatedAt: Date;
    cvv: number;
    expiredMonth: number;
    expiredYear: number;
    nickname: string;
    name: string;
    _id: string;
  }[]
) => {
  const hideInfo = (
    string: string | number | undefined,
    cvv: boolean = false
  ) => {
    if (!string) {
      return;
    }

    const stringified = String(string);

    if (stringified.length === 3 && cvv) {
      return "***";
    }

    return `${stringified.slice(0, 4)}*********${stringified.slice(
      stringified.length - 4
    )}`;
  };

  const isDataArray = Array.isArray(info);

  const mappedArray = (Array.isArray(info) ? info : [info]).map((el) => {
    const {
      cvv,
      name,
      cardNumber,
      createdAt,
      updatedAt,
      nickname,
      _id,
      expiredMonth,
      expiredYear,
    } = el;

    const cardType = creditCardType(cardNumber)[0];

    return {
      createdAt,
      updatedAt,
      nickname,
      id: _id,
      expiredMonth,
      expiredYear,
      cvv: hideInfo(cvv, true),
      cardNumber: hideInfo(cardNumber),
      name: hideInfo(name),
      cardType: cardType ? cardType.niceType : "",
    };
  });

  return isDataArray ? mappedArray : mappedArray[0];
};

export default { getCardList, createCard, getCard };
