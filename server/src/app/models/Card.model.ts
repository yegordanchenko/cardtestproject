import mongoose from "mongoose";
const Schema = mongoose.Schema;

let CardSchema = new Schema(
  {
    name: { type: String, default: "", required: true },
    nickname: { type: String, default: "" },
    cardNumber: { type: String, required: true },
    expiredMonth: { type: Number, required: true },
    expiredYear: { type: Number, required: true },
    cvv: { type: Number, required: true },
  },
  { versionKey: false, timestamps: true }
);

CardSchema.set("toJSON", {
  transform: function (doc, ret, options) {
    ret.id = ret._id;
    delete ret._id;
  },
});

export default mongoose.model("Card", CardSchema);
